extern crate gettextrs;
use gettextrs::*;

fn main() {
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain("test", "./po");
    textdomain("test");

    println!("Original: Open");
    println!("Translated: {}", gettext("Open"));

    println!("Original: Text Editor");
    println!("Translated singular: {}", ngettext("Text Editor", "Text Editors", 1));
    println!("Translated plural: {}", ngettext("Text Editor", "Text Editors", 2));
}
